FROM node:18-alpine AS BASE_IMAGE

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app

# Install dependencies
COPY package.json ./
RUN npm install --omit=dev && \
    npm install typescript && \
    npm cache clean --force

# Build app
COPY src tsconfig.json tslint.json ./
RUN npm run build

FROM node:18-alpine
WORKDIR /usr/src/app

COPY package.json ./
COPY --from=BASE_IMAGE /usr/src/app/dist ./dist
COPY --from=BASE_IMAGE /usr/src/app/node_modules ./node_modules

CMD npm start
