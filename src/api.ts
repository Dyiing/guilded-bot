import fetch from "node-fetch";

export interface oAuthResponse {

    status: string;
    message: string;
    username?: string;
    uuid?: string;

}

export interface AristoisUser {

    userId?: string;
    username?: string;
    uuid?: string;
    properties?: UserProperties;
    admin?: boolean;
    message?: string;

}

export interface UserProperties {

    banned: boolean;
    donor: boolean;
    prefix?: string;

}

export async function oAuthToUUID(token: string) {
    return <oAuthResponse> await fetch(`https://auth.aristois.net/token/${token}`).then(response => response.json());
}

export async function getAristoisUser(uuid: string, username: string) {
    try {
        const data = await fetch(`https://api.aristois.net/v3/user/${uuid}`, {
            method: "GET",
            headers: {
                authorization: `Bearer ${process.env.API_KEY}`,
                username: username
            }
        });
        return <AristoisUser> await data.json();
    } catch (err) {
        console.error(`[Aristois API] Failed to fetch user ${uuid} (${username})`, err);
    }
}
