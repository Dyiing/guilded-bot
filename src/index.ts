import { Client, Embed, Message, Role} from "guilded.js";
import { readFileSync } from "fs";
import { resolve } from "path";
import { getAristoisUser, oAuthToUUID } from "./api";
import { config } from "dotenv";

config();

interface appConfig {
    prefix: string;
    botID: string;
    guildID: string;
    roleID: number;
    channelID: string;
}

class Bot {
    private client = new Client({token: process.env.TOKEN!});
    private config: appConfig

    constructor(config: appConfig) {
        this.config = config;        
        this.client.on('ready', () => {
            console.log(`Bot successfully loaded with user ID ${this.client.user?.id}.`);
            console.log(`Prefix is ${config.prefix}`);
        });

        this.client.on("messageCreated", this.onMessageCreate.bind(this));

        this.client.login();
    }

    async onMessageCreate(message: Message) {
        if (message.authorId === this.config.botID) 
            return;

        const content = message.content;
        let author = message.authorId;
        if (content.startsWith(`${this.config.prefix}donor`)) {
            if(message.channelId !== this.config.channelID) {
                this.client.messages.send(message.channelId, new Embed()
                .setTitle(":no_entry_sign:")
                .setDescription("To get the donor role, please visit [**#donor-role**](https://www.guilded.gg/aristois/groups/PdYbEW7z/channels/63a70deb-e873-46a8-99e8-79d9defc1bfa/chat) and follow the instructions there.")
                .setColor("#9A006C"));
                return;
            } else {
                await this.client.messages.send(message.channelId, { embeds: [new Embed().setDescription(`<@${author}>,\n The \`!donor\` command is no longer used to get the donor role. Please read above for instructions.`).setFooter(`This message will be deleted after 1 minute.`)], isPrivate: true})
                .then(message => {
                    setTimeout(() => message.delete(), 60000)
                  })
                message.delete();
                return;
            }
            
        } else if(message.channelId !== this.config.channelID) {
            return;

        } else {
            //Too long or includes non-numerical characters
            if (!/^[0-9]{6}$/.test(content)) {
                await this.client.messages.send(message.channelId, { embeds: [new Embed().setDescription(`<@${author}>,\n Invalid format: Send your 6-digit auth token and nothing else. (Example: 123456) | Join the Minecraft server \`auth.aristois.net\` to receive an auth token.`).setFooter(`This message will be deleted after 1 minute.`)], isPrivate: true})
                .then(message => {
                    setTimeout(() => message.delete(), 60000)
                  })
                message.delete();
                return;
            }

            //Invalid token
            const meta = await oAuthToUUID(content);
            if (!meta.uuid || !meta.username) {
                await this.client.messages.send(message.channelId, { embeds: [new Embed().setDescription(`<@${author}>,\n The auth token used is invalid. Please try again with a new token.`).setFooter(`This message will be deleted after 2 minutes.`)], isPrivate: true})
                .then(message => {
                    setTimeout(() => message.delete(), 120000)
                  })
                message.delete();
                return;
            }

            //Valid token
            const aristoisUser = await getAristoisUser(meta.uuid, meta.username);
            if (aristoisUser?.properties?.donor) {
                let drole = new Role(this.client, {id: this.config.roleID,
                    serverId: this.config.guildID})

                let author = message.authorId
                drole.assignToMember(author);

                await this.client.messages.send(message.channelId, { embeds: [new Embed().setDescription(`<@${author}>,\n Success! You've now been given the donor role.`).setFooter(`This message will be deleted after 2 minutes.`)], isPrivate: true})
                .then(message => {
                    setTimeout(() => message.delete(), 120000)
                  })
                message.delete();
            } else {
                await this.client.messages.send(message.channelId, { embeds: [new Embed().setDescription(`<@${author}>,\n The account authenticated is not a donor. Ensure you use the correct account when getting a token.`).setFooter(`This message will be deleted after 2 minutes.`)], isPrivate: true})
                .then(message => {
                    setTimeout(() => message.delete(), 120000)
                  })
                message.delete();
            }
        }

    }

}

if (require.main === module) {
    const path = resolve("config.json");
    const config = <appConfig> JSON.parse(readFileSync(path).toString());
    const bot = new Bot(config);
}
